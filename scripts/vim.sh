#!/bin/bash

set -ex

mkdir -p ~/.vim/bundle
if [ -e ~/.vim/bundle/Vundle.vim ]
then
  :
else
  git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
fi
/usr/bin/vim +PluginInstall +qall
