#!/bin/bash

set -eu

export DEBIAN_FRONTEND=noninteractive

vers=$(apt-cache search nvidia-dkms | grep -oE 'dkms-[0-9]+' | sort -ur | cut -c6- | head -1)
nvidia=(
  "nvidia-dkms-${vers}"
  "libnvidia-gl-${vers}"
  "nvidia-driver-${vers}"
  "nvidia-utils-${vers}"
  "libnvidia-common-${vers}"
  "libnvidia-compute-${vers}"
  "libnvidia-decode-${vers}"
  "libnvidia-encode-${vers}"
  "libnvidia-cfg1-${vers}"
  "xserver-xorg-video-nvidia-${vers}"
)

_help()
{
   # Display Help
   echo "Nvidia drivers updater."
   echo "r     Run APT to update kernel & modules."
   echo "d     Dry-run APT to update kernel & modules."
   echo "h     Print this Help."
   echo
}

while getopts ":dr" option; do
  case $option in
    d) # dryrun
      echo "Drivers version: ${vers}"
      apt-get install --dry-run "${nvidia[@]}"
      apt-get autoremove
      apt-get clean
      exit;;
    r) # run
      apt-get install -y "${nvidia[@]}"
      apt-get autoremove
      apt-get clean
      exit;;
    *) # display Help
      _help
      exit;;
   esac
done
