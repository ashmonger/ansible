#!/bin/bash

set -eu

export DEBIAN_FRONTEND=noninteractive

vers=$(apt-cache search linux-image-5 | grep generic | grep -oE '([0-9]+\.){2}[0-9]\-[0-9]{2}' | sort -rg -k2 -t. | head -1)
subvers=$(cut -d'.' -f-2 <<< "$vers")
kernel=(
  "linux-headers-${vers}-generic"
  "linux-hwe-${subvers}-headers-${vers}"
  "linux-image-${vers}-generic"
  "linux-modules-${vers}-generic"
  "linux-modules-extra-${vers}-generic"
)

_help()
{
   # Display Help
   echo "Kernel and modules updater."
   echo "r     Run APT to update kernel & modules."
   echo "d     Dry-run APT to update kernel & modules."
   echo "h     Print this Help."
   echo
}

while getopts ":dr" option; do
  case $option in
    d) # dryrun
      echo "${vers}-${subvers}"
      apt-get install --dry-run "${kernel[@]}"
      apt-get autoremove
      apt-get clean
      exit;;
    r) # run
      apt-get install -y "${kernel[@]}"
      apt-get autoremove
      apt-get clean
      exit;;
    *) # display Help
      _help
      exit;;
   esac
done
