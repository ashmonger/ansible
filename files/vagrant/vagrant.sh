#!/bin/bash

set -euo pipefail

function vms(){
        home="/srv/vagrant/vms"
        shopt -s globstar nullglob
        for vm in "$home"/**/Vagrantfile
        do
                cd "$(dirname "$vm")" || exit
                vagrant "${1}"
        done
}

export -f vms

case $1 in
start)
        vms up
        exit 0
        ;;
stop)
        vms halt
        exit 0
        ;;
*)
        echo "error: no \$1"
        exit 1
        ;;
esac
