#!/bin/bash

set -euo pipefail

date=$(date +%Y-%m-%d)

chatid=854433310
token=BOTKEY

curl -sS -X POST "https://api.telegram.org/$token/sendMessage" --data chat_id="$chatid" --data text="Bitwarden Save Start"
cd /opt
systemctl stop bitwarden.service
tar czvf "/opt/saves/bitwarden-$date.tar.gz" /opt/bitwarden
tar czvf "/opt/saves/web-vault-$date.tar.gz" /opt/web-vault
/usr/bin/sqlite3 /opt/bitwarden/data/db.sqlite3 ".backup /opt/saves/bw_backup_$date.sqlite3"
systemctl start bitwarden.service
curl -sS -X POST "https://api.telegram.org/$token/sendMessage" --data chat_id="$chatid" --data text="Bitwarden Save Done"

curl -sS -X POST "https://api.telegram.org/$token/sendMessage" --data chat_id="$chatid" --data text="Rsync to US Begin"
rsync -e ssh -avz /opt/save/. us.lamethode.xyz:/opt/save
curl -sS -X POST "https://api.telegram.org/$token/sendMessage" --data chat_id="$chatid" --data text="Rsync to US Done"
